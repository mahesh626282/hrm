package com.jsp.hrm.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ObjectMapperUtil<E> {

	@Autowired
	private ObjectMapper objectMapper;

	public E getJavaObject(String json, Class<E> javaType)
	
			throws JsonMappingException, JsonProcessingException, ClassCastException {

		return (E) objectMapper.readValue(json, javaType);

	}

//	public List<E> getJavaObject(String jsonList, Class<E> javaType) {
//	
//		return  (List<E>) objectMapper.readValue(jsonList, javaType);
//	}
//	

}
