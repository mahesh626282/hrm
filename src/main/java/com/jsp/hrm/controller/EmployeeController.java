package com.jsp.hrm.controller;

import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jsp.hrm.constants.AppConstants;
import com.jsp.hrm.dto.LeaveApplicationDto;
import com.jsp.hrm.dto.LeaveDetailsDto;
import com.jsp.hrm.dto.ResponseDto;
import com.jsp.hrm.entity.AuditTable;
import com.jsp.hrm.entity.EmployeeEntity;
import com.jsp.hrm.service.AuditTableService;
import com.jsp.hrm.service.EmployeeService;

@RestController
@RequestMapping(value = AppConstants.EMPLOYEE_CONTROLLER)
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

//	@Autowired
//	private AuditTableService auditTableService;
//	
//	@Autowired
//	private HttpServletRequest request;

	@PostMapping(value = AppConstants.SAVE_EMPLOYEE)
	public ResponseDto save(@RequestBody EmployeeEntity employee) {
		System.out.println("hi from controller class save method");
		EmployeeEntity employee1 = employeeService.saveEmployee(employee);
		if (employee1 != null) {

			// System.out.println("request is "+ request.getRequestURI());

			
			System.out.println("hi from akhilesh");
			ResponseDto respo = new ResponseDto(HttpStatus.ACCEPTED.name(), HttpStatus.ACCEPTED.value(), employee1,
					null);

			// AuditTable audit = new
			// AuditTable("http://localhost:8080/employeeController/saveEmployee",ResponseDto.class,employee.toString(),respo.toString(),"succuss");

			// System.out.println(audit);
			// auditTableService.saveAuditTable(audit);
			return respo;
		} else {
			return new ResponseDto(HttpStatus.NOT_ACCEPTABLE.name(), HttpStatus.UNPROCESSABLE_ENTITY.value(), employee1,
					"improcessable entity");
		}

	}

	@GetMapping(value = AppConstants.GET_ALL_EMPLOYEES)
	public @ResponseBody ResponseDto getAllEmployees() {
		try {

			List<EmployeeEntity> allEmployeeList = employeeService.getAllEmployeeList();

			ResponseDto response = new ResponseDto(HttpStatus.ACCEPTED.name(), HttpStatus.ACCEPTED.value(),
					allEmployeeList, null);

//			AuditTable audit = new AuditTable(request.getRequestURI(),ResponseDto.class,null,response.toString(),"succuss");
//			System.out.println(audit);
//			auditTableService.saveAuditTable(audit);
			return response;
		} catch (Exception e) {

			ResponseDto response = new ResponseDto(HttpStatus.BAD_REQUEST.name(), HttpStatus.NOT_FOUND.value(), null,
					"bad request");

			return response;

		}
	}

	@GetMapping(value = AppConstants.FIND_BY_ID)
	public @ResponseBody ResponseDto getById(@PathVariable("id") Long id) {

		try {

			EmployeeEntity employee = employeeService.getById(id);

			ResponseDto response = new ResponseDto(HttpStatus.FOUND.name(), HttpStatus.FOUND.value(), employee, null);
			return response;

		} catch (NoSuchElementException n) {
			ResponseDto response = new ResponseDto(HttpStatus.NOT_FOUND.name(), HttpStatus.NOT_FOUND.value(), null,
					"emplpoyee not prasent by that id");
			return response;
			// TODO: handle exception
		} catch (Exception e) {

			ResponseDto response = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.name(),
					HttpStatus.INTERNAL_SERVER_ERROR.value(), null, e.toString());
			return response;
		}

	}

	@GetMapping(value = AppConstants.DELETE_BY_ID)
	public @ResponseBody ResponseDto deleteEmployeeById(@PathVariable("id") Long id) {
		try {

			employeeService.deleteEmployeeById(id);
			ResponseDto response = new ResponseDto(HttpStatus.I_AM_A_TEAPOT.name(), HttpStatus.I_AM_A_TEAPOT.value(),
					id, null);
			return response;

		} catch (Exception e) {

			ResponseDto response = new ResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.name(),
					HttpStatus.INTERNAL_SERVER_ERROR.value(), id, "internal server error");
			return response;

			// TODO: handle exception
		}
	}

	@GetMapping(value = AppConstants.FIND_BY_NAME)
	public @ResponseBody ResponseDto getByName(@RequestHeader("fullName") String fullName) {

		try {
			List<EmployeeEntity> employee = employeeService.getByName(fullName);
			ResponseDto repsponse = new ResponseDto(HttpStatus.ACCEPTED.name(), HttpStatus.ACCEPTED.value(), employee,
					null);
			return repsponse;
		} catch (Exception e) {
			ResponseDto resp = new ResponseDto(HttpStatus.BAD_REQUEST.name(), HttpStatus.BAD_GATEWAY.value(), null,
					"internal server error");
			return resp;

		}
	}

	@GetMapping(value = AppConstants.FIND_BY_NAME_AND_STATUS)
	public @ResponseBody ResponseDto getByNameAndStatus(@RequestHeader("name") String fullName,
			@RequestHeader("status") String status) {

		try {
			List<EmployeeEntity> employee = employeeService.getByNameAndStatus(fullName, status);
			ResponseDto repsponse = new ResponseDto(HttpStatus.ACCEPTED.name(), HttpStatus.ACCEPTED.value(), employee,
					null);
			return repsponse;
		} catch (Exception e) {
			ResponseDto resp = new ResponseDto(HttpStatus.BAD_REQUEST.name(), HttpStatus.BAD_GATEWAY.value(), null,
					"internal server error");
			return resp;

		}
	}

	@GetMapping(value = AppConstants.GET_ALL_EMPLOYEES_NAMES)
	public @ResponseBody List<String> getEmployeesName() {
		return employeeService.getEmployeesNames();
	}

	@PostMapping(value = AppConstants.LEAVE_REQUEST)
	public @ResponseBody ResponseDto leaveRequest(@RequestBody LeaveApplicationDto leaveAppDto) {
		return employeeService.processLeaveRequest(leaveAppDto);
	}

	
	@GetMapping(value = AppConstants.GET_ALL_EMPLOYEES_LEAVE_DETAILS)
	public @ResponseBody List<LeaveDetailsDto> getAllEmployessLeaveDetails() {
		 return employeeService.getAllEmployeeLeaveDetails();
	}
	
	@GetMapping(value = AppConstants.GET_EMPLPOYEE_LEAVE_DETAILS_BY_ID)
	public @ResponseBody List<LeaveDetailsDto> getLeaveDetilsByEmpId(@RequestParam("id") Long id) {
	return	employeeService.getLeaveDetailsById(id);	
	}
	
	@GetMapping(value = AppConstants.GET_EMPLOYEE_ADDRESS_BY_ID)
	public @ResponseBody String getEmployeeAddressById(@RequestParam Long id) {
		return employeeService.getEmployeeAddressById(id);
	}
}
