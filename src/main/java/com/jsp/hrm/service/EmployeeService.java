package com.jsp.hrm.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsp.hrm.dto.LeaveApplicationDto;
import com.jsp.hrm.dto.LeaveDetailsDto;
import com.jsp.hrm.dto.ResponseDto;
import com.jsp.hrm.entity.EmployeeEntity;
import com.jsp.hrm.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public EmployeeEntity saveEmployee(EmployeeEntity employee) {
		EmployeeEntity save = employeeRepository.save(employee);
		return save;

	}

	public List<EmployeeEntity> getAllEmployeeList() {
		List<EmployeeEntity> findAll = employeeRepository.findAll();
		return findAll;
	}

	public EmployeeEntity getById(Long id) {
		Optional<EmployeeEntity> optional = employeeRepository.findById(id);
		EmployeeEntity employeeEntity = optional.get();
		return employeeEntity;
	}

	public void deleteEmployeeById(long id) {
		employeeRepository.deleteById(id);
	}

	public List<EmployeeEntity> getByName(String name) {
		return employeeRepository.findByFullName(name);
	}

	public List<EmployeeEntity> getByNameAndStatus(String name, String status) {
		return employeeRepository.findByFullNameAndStatus(name, status);
	}

	public List<String> getEmployeesNames() {
		return employeeRepository.findEmployeesName();
	}

	public ResponseDto processLeaveRequest(LeaveApplicationDto leaveAppDto) {

		HttpEntity<LeaveApplicationDto> http = new HttpEntity<>(leaveAppDto);
		String url = "http://localhost:8081/leaveController/saveLeaveDetails";
		ResponseEntity<ResponseDto> exchange = restTemplate.exchange(url, HttpMethod.POST, http, ResponseDto.class);

		ResponseDto responseDto = exchange.getBody();

		return responseDto;

	}

	public List<LeaveDetailsDto> getAllEmployeeLeaveDetails() {

		ResultSetExtractor<List<LeaveDetailsDto>> res = new ResultSetExtractor<List<LeaveDetailsDto>>() {

			@Override
			public List<LeaveDetailsDto> extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<LeaveDetailsDto> list = new LinkedList<>();

				while (rs.next()) {
					LeaveDetailsDto al = new LeaveDetailsDto();
					al.setId(rs.getLong("id"));
					al.setFullName(rs.getString("full_name"));
					al.setStartDate(rs.getDate("start_date"));
					al.setEndDate(rs.getDate("end_date"));
					al.setTotalDays(rs.getInt("total_days"));
					list.add(al);

				}
				return list;
			}

		};

		List<LeaveDetailsDto> list = jdbcTemplate.query("SELECT e.id,e.full_name,l.start_date,l.end_date,l.total_days "
				+ "FROM employee_info e left join leave_details l " + "on e.id=l.emp_id;", res);

		return list;
	}

	public List<LeaveDetailsDto> getLeaveDetailsById(Long id) {
		
		List<LeaveDetailsDto> list = getAllEmployeeLeaveDetails();

		List<LeaveDetailsDto> collect = list.stream().filter((m) -> {
			return m.getId() == id;
		}).collect(Collectors.toList());

		return collect;

	}
	
	public String getEmployeeAddressById(Long id) {
		 String address = employeeRepository.findEmployeeAddressById(id);
		
		return address;
	}

}
