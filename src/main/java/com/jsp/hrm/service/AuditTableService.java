package com.jsp.hrm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jsp.hrm.entity.AuditTable;
import com.jsp.hrm.repository.AuditTableRepository;

@Service
public class AuditTableService {

	@Autowired
	private AuditTableRepository auditTableRepository;
	
	public void saveAuditTable(AuditTable auditTable) {
		auditTableRepository.save(auditTable);
		
	}
}
