package com.jsp.hrm.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class LeaveDetailsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long id;
	

	private String fullName;
	
	
	private Date startDate;
	
	
	private Date endDate;
	

	private int totalDays;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public int getTotalDays() {
		return totalDays;
	}


	public void setTotalDays(int totalDays) {
		this.totalDays = totalDays;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	
}

