package com.jsp.hrm.dto;



import java.util.Date;

import lombok.Data;

@Data
public class LeaveApplicationDto {
	
	private String empId;
	
	private Date startDate;
	
	private Date endDate;
 
}
