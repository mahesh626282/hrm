package com.jsp.hrm.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AddressDto implements Serializable {

	private String addressLine;
	private String city;
	private String country;
	private String pinCode;
	private String landMark;
	private boolean isCurrentAddress;
	private String state;
	
	
}
