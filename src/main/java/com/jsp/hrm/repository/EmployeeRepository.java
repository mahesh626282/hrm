package com.jsp.hrm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jsp.hrm.entity.EmployeeEntity;


public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
	
	
	//@Query("from EmployeeEntity where name=:name")
	public List<EmployeeEntity> findByFullName(String name);
	
	
	public List<EmployeeEntity> findByFullNameAndStatus(String name,String status);
	
	@Query(nativeQuery = true,value = "select fullName from employee_info")
	public List<String> findEmployeesName();
	
	@Query(nativeQuery = true,value = "select address from employee_info where id=:id")
	public String findEmployeeAddressById(Long id);
}
