package com.jsp.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jsp.hrm.entity.AuditTable;

public interface AuditTableRepository extends JpaRepository<AuditTable, Long>{

}
