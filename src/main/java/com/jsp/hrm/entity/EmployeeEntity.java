package com.jsp.hrm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jsp.hrm.constants.AppConstants;


import lombok.Data;

//@NoArgsConstructor
//@EqualsAndHashCode
//@ToString
//@Getter
//@Setter

/*
 * @data will combine all the above annotation from lambok to do work like
 * generate getters and setters and to ovcerride equals and hashcode and to override tostring etc....*/

@Data
@Entity
@Table(name = AppConstants.EMPLOYEE_TABLE_NAME)
public class EmployeeEntity implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "auto", strategy ="increment" )
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private Long id;

	@Column(name = "fullName")
	private String fullName;

	@Column(name = "email")
	private String email;

	@Column(name = "role")
	private String role;

	@Column(name = "employee_start_date")
	private Date employeeStartDate;

	@Column(name = "status")
	private String status;

	@Column(name = "address")
	private String Address;
	

	@Column(name = "gender",nullable =false)
	private String gender;

	@Column(name = "bond")
	private String bond;

	
	

}
