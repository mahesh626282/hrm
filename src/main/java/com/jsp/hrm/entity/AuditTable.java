package com.jsp.hrm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
public class AuditTable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name = "auto",strategy = "increment")
	@GeneratedValue(generator = "auto")
	private Long id;
	
	private String requestUrl;
	
	private Class<?> returnType;
	
	private String requestBody;
	
	private String returnBody;
	
	private String status;
	
	private Date createdDate;

	public AuditTable() {
		super();
		createdDate=new Date();
		// TODO Auto-generated constructor stub
	}

	public AuditTable( String requestUrl, Class<?> returnType, String requestBody, String returnBody,
			String status) {
		super();
		this.requestUrl = requestUrl;
		this.returnType = returnType;
		this.requestBody = requestBody;
		this.returnBody = returnBody;
		this.status = status;
		this.createdDate=new Date();
	}
	
}
