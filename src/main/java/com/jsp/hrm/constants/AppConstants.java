package com.jsp.hrm.constants;

public interface AppConstants {

	String EMPLOYEE_TABLE_NAME="employee_info";
	
	
	String EMPLOYEE_CONTROLLER="/employeeController";
	
	String SAVE_EMPLOYEE="/saveEmployee";
	
	String GET_ALL_EMPLOYEES="/getAllEmployees";
	
	String FIND_BY_ID="/findById/{id}";
	
	//id will be passed as part of url.
	String DELETE_BY_ID="/deleteEmployeeById/{id}";

	//name will be passed as part of header
	String FIND_BY_NAME="/getByName";
	
	String FIND_BY_NAME_AND_STATUS="/getByNameAndStatus";
	
	
	String LEAVE_REQUEST="/leaveRequest";
	
	
	String GET_ALL_EMPLOYEES_NAMES="/getEmployeesName";
	
	
	String GET_ALL_EMPLOYEES_LEAVE_DETAILS="/getAllEmployessLeaveDetails";
	
	String GET_EMPLPOYEE_LEAVE_DETAILS_BY_ID="/getLeaveDetilsByEmpId";
	
	String GET_EMPLOYEE_ADDRESS_BY_ID="getEmployeeAddressById";

}
